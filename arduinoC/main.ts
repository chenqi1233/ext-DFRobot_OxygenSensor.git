//% color="#00b4f6" iconWidth=50 iconHeight=40
namespace DFRobot_OxygenSensor{

    //% block="I2C Oxygen Concentration Sensor Initial Address: [ADDRESS] Waits Until Success" blockType="command" 
    //% ADDRESS.shadow="dropdown" ADDRESS.options="ADDRESS"
    
    export function DFRobot_OxygenSensorInit(parameter: any, block: any) {
         let address = parameter.ADDRESS.code;
        
     

        Generator.addInclude("DFRobot_OxygenSensorInit", "#include <DFRobot_OxygenSensor.h>");
        
        Generator.addObject("DFRobot_OxygenSensorInit","DFRobot_OxygenSensor",`Oxygen;`);
  
        Generator.addSetup(`DFRobot_OxygenSensorInit`, `while(!Oxygen.begin(${address}));`);
     
        
    }
    //% block="I2C Oxygen Concentration Sensor Reads Ambient Oxygen Concentration for [COLLECT_NUMBER] averages" blockType="reporter" 
    //% COLLECT_NUMBER.shadow="range" COLLECT_NUMBER.defl="10" COLLECT_NUMBER.params.min="1" COLLECT_NUMBER.params.max="100" 
    export function DFRobot_OxygenSensorReady(parameter: any, block: any) { 
        let number = parameter.COLLECT_NUMBER.code;
        
        Generator.addCode( [`Oxygen.ReadOxygenData(${number})`,Generator.ORDER_UNARY_POSTFIX]);
   
   }
  
   

}
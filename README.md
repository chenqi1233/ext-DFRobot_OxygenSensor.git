# Gravity：I2C氧气浓度传感器

DFRobot最新推出IIC氧气浓度传感器，有效量程为0~25%Vol。该款传感器利用电化学原理对空气中的氧气浓度进行监测，具有高精度、高灵敏度、线性范围宽、抗干扰能力强以及优异的重复性和稳定性的特点。使用IIC接口，就可读取传感器所在环境中的氧气浓度，可以兼容各类单片机和传感器，使用非常简单。 该氧气传感器可广泛应用于工业、矿井、仓储等空气不易流通的空间，以及环保领域中的氧气浓度检测。

商品链接：https://www.dfrobot.com.cn/goods-2792.html
 
![](./arduinoC/_images/featured.png)

# 积木

- 上传模式：   
![](./arduinoC/_images/blocks.png)

- Python模式行空板：   
![](./python/_images/blocks.png)

# 程序实例

  

![](./arduinoC/_images/example.png)

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|Python|
|-----|-----|:-----:|-----|-----|
|uno||√|||
|micro:bit||√|||
|mpython||√|||
|arduinonano|||||
|leonardo|||||
|mega2560|||||
|unihiker||||√|

# 更新日志

* V0.0.1 基础功能完成
* V0.0.2 增加python模式支持

